<?php
	$connection = null;

	/**********
	*** YOUR CODE HERE
	**********/
	use Neoxygen\NeoClient\ClientBuilder;

	require 'vendor/autoload.php';
	 
	$connection = ClientBuilder::create()
	  ->addConnection('default', 'http', 'localhost', 7474)
	  ->setAutoFormatResponse(true)
	  ->build();
?>
